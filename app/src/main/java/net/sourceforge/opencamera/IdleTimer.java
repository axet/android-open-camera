package net.sourceforge.opencamera;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

class IdleTimer extends BroadcastReceiver {
    public static String TAG = IdleTimer.class.getSimpleName();

    public static IdleTimer timer;

    Activity activity;
    long t;
    Handler handler = new Handler();

    public static void onCreate(Activity a) {
        if (timer != null && timer.activity != a) {
            timer.close();
            timer = null;
        }
        if (timer == null) {
            timer = new IdleTimer();
            timer.create(a);
        }
    }

    public static void onUserInteraction() {
        timer.featureScreenOff();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, intent.toString());
        String a = intent.getAction();
        if (a == null)
            return;
        if (a.equals(Intent.ACTION_SCREEN_OFF))
            activity.finish();
    }

    public void create(Activity a) {
        this.activity = a;
        t = 30 * 1000; // 30 seconds (android default)
        try {
            t = Settings.System.getInt(a.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT);
        } catch (Settings.SettingNotFoundException e) {
        }
        featureHideActivity();
        featureScreenOff();
    }

    public void close() {
        activity.unregisterReceiver(this);
    }

    public void featureHideActivity() { // remove activity after screen off, to prevent camera view
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(activity);
        boolean o = shared.getBoolean(PreferenceKeys.KeepDisplayOnPreferenceKey, true);
        boolean l = shared.getBoolean(PreferenceKeys.ShowWhenLockedPreferenceKey, true);
        if ((t > 30 * 1000 || o) && l) {
            IntentFilter ff = new IntentFilter();
            ff.addAction(Intent.ACTION_SCREEN_OFF);
            activity.registerReceiver(this, ff);
        }
    }

    public void featureScreenOff() { // turnoff screen after delay
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onReceive(activity, new Intent(Intent.ACTION_SCREEN_OFF));
            }
        }, t);
    }
};
